Gem::Specification.new do |s|
  s.name = %q{api-request}
  s.version = "0.0.1"
  s.authors = ['Habib Nurrahman']
  s.license = 'MIT'
  s.email = 'nurmanhabib@gmail.com'
  s.date = %q{2019-10-03}
  s.homepage = 'https://gitlab.com/nurmanhabib-gems/api-request'
  s.summary = %q{API Request library}
  s.files = [
      "lib/request_sender.rb",
      "lib/error_request_sender.rb"
  ]
  s.require_paths = ["lib"]
end
