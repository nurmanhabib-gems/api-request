module ApiRequest
  module Request
    class HttpRequest
      def send

      end

      def after_success(data)

      end

      def after_error(data, err)

      end

      def handle_response(response)
        data = JSON.parse(response)

        after_success data['data']

        data['data']
      rescue
        raise ApiRequest::ErrorRequestSender.new(self.class.name, response, {}, 500)
      end

      def handle_error(response, err)
        data = JSON.parse(response)

        after_error data, err

        default_message = Rack::Utils::HTTP_STATUS_CODES[err.http_code]
        error_response = data['message'].present? ? data['message'].to_s : default_message

        raise ApiRequest::ErrorRequestSender.new(self.class.name, error_response, data['errors'], err.http_code)
      rescue => e
        Rollbar.error(e)
        raise ApiRequest::ErrorRequestSender.new(self.class.name, response, {}, 500)
      end
    end
  end
end
