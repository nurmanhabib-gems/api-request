module ApiRequest
  class ErrorRequestSender < StandardError
    attr_reader :request, :errors, :status

    def initialize(request, message, errors = {}, status = 500)
      super message
      @request = request
      @errors = errors
      @status = status
    end
  end
end
