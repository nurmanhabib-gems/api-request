class ApiRequest::RequestSender
  class << self
    def send(request)
      if request.present?
        begin
          request.handle_response(request.send)
        rescue RestClient::ExceptionWithResponse => err
          response = err.response.present? ? err.response : err.message
          request.handle_error(response, err)
        rescue StandardError => err
          request.handle_error(err.message, err)
        end
      end
    end
  end

  def initialize(requests = [])
    @requests = requests
  end

  def <<(request)
    @requests << request
  end

  def >>(request)
    ApiRequest::RequestSender::send(request)
  end

  def send
    @requests.each do |request|
      ApiRequest::RequestSender::send(request)
    end
  end
end
